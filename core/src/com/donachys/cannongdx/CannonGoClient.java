package com.donachys.cannongdx;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.donachys.cannongdx.CannonGoGrpc.CannonGoBlockingStub;
import com.donachys.cannongdx.CannonGoGrpc.CannonGoStub;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;

public class CannonGoClient {
    private StreamObserver<StreamingPlayRequest> streamObserver;

    private static final String defaultHost = "localhost";
    private static final int defaultPort = 55531;

    private static final Logger logger = Logger.getLogger(CannonGoClient.class.getName());

    private final ManagedChannel channel;
    private final CannonGoBlockingStub blockingStub;
    private CannonGoStub asyncStub;
    private LinkedBlockingQueue responses;

    private String user;
    private String secret;

    private final Metadata.Key<String> NAMEKEY = Metadata.Key.of("user", Metadata.ASCII_STRING_MARSHALLER);
    private final Metadata.Key<String> SECRETKEY = Metadata.Key.of("secret", Metadata.ASCII_STRING_MARSHALLER);

    /** Construct client for accessing CannonGoClient server at {@code host:port}. */
    public CannonGoClient(String host, int port) {
        this(ManagedChannelBuilder.forTarget("dns:///"+host+":"+port).usePlaintext());

    }
    public CannonGoClient() {
        this(ManagedChannelBuilder.forAddress(CannonGoClient.defaultHost, CannonGoClient.defaultPort).usePlaintext());
    }

    /** Construct client for accessing CannonGoClient server using the existing channel. */
    public CannonGoClient(ManagedChannelBuilder<?> channelBuilder) {
        channel = channelBuilder.build();
        blockingStub = CannonGoGrpc.newBlockingStub(channel);
        asyncStub = CannonGoGrpc.newStub(channel);
        responses = new LinkedBlockingQueue<StreamingPlayResponse>();
    }
    public void applyAsyncMetadata(Metadata md) {
        asyncStub = MetadataUtils.attachHeaders(asyncStub, md);
    }
    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public JoinResponse join(String name) {
        JoinRequest req = JoinRequest.newBuilder().setUserName(name).build();
        return blockingStub.join(req);
    }

    public void submitRequest(StreamingPlayRequest req) {
//        System.out.println("submitRequest called, streamobserver: " + streamObserver);
        if (streamObserver == null) {
            //TODO: exception?
            return;
        }
        try {
//            System.out.println("submitting req" + req);
            streamObserver.onNext(req);
        } catch (RuntimeException e) {
            // Cancel RPC
            streamObserver.onError(e);
            throw e;
        }
    }

//
//
//    public void streamComplete() {
//        streamObserver.onCompleted();
//    }

    public CountDownLatch streamingPlay(final CannonGame cg){
//        info("streamingPlay");

        Metadata headers = new Metadata();
        headers.put(NAMEKEY, user);
        headers.put(SECRETKEY, secret);
        applyAsyncMetadata(headers);

        final CountDownLatch finishLatch = new CountDownLatch(1);
        streamObserver = asyncStub.streamingPlay(new StreamObserver<StreamingPlayResponse>() {
                    @Override
                    public void onNext(StreamingPlayResponse value) {
//                        info("Got message \"{0}\"", value.getGameState());
                        cg.handleStreamingPlayResponse(value);
                    }

                    @Override
                    public void onError(Throwable t) {
                        warning("streamingPlay Failed: {0}", Status.fromThrowable(t));
                        finishLatch.countDown();
                    }

                    @Override
                    public void onCompleted() {
//                        info("Finished streamingPlay");
                        finishLatch.countDown();
                    }
                });
        return finishLatch;
    }

    public void setUser(String u) {
        user = u;
    }

    public void setSecret(String s) {
        secret = s;
    }

    private void info(String msg, Object... params) {
        logger.log(Level.INFO, msg, params);
    }

    private void warning(String msg, Object... params) {
        logger.log(Level.WARNING, msg, params);
    }

    public StreamingPlayRequest newReadyPlayRequest(boolean ready) {
        return StreamingPlayRequest.newBuilder().setRAction(
                StreamingPlayRequest.newBuilder().getRActionBuilder().setReady(ready)
        ).build();
    }

    public StreamingPlayRequest newFirePlayRequest(float rot, float pow, float pitch) {
        return StreamingPlayRequest.newBuilder().setFAction(
                StreamingPlayRequest.newBuilder().getFActionBuilder().
                    setRotation(rot).setPower(pow).setPitch(pitch)
        ).build();
    }
}
