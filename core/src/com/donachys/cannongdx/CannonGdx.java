package com.donachys.cannongdx;

import com.badlogic.gdx.Game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.donachys.cannongdx.Screens.TitleScreen;

public class CannonGdx extends Game {
	public static Skin skin;
	public int height, width;

	public CannonGdx(int width, int height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public void create () {
		skin = new Skin(Gdx.files.internal("holo/skin/dark-hdpi/Holo-dark-hdpi.json"));
		this.setScreen(new TitleScreen(this));
	}
}
