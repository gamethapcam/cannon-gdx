package com.donachys.cannongdx.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.donachys.cannongdx.CannonGdx;
import com.donachys.cannongdx.CannonGoClient;
import com.donachys.cannongdx.JoinResponse;

public class TitleScreen implements Screen {
    private Stage stage;
    private CannonGdx game;
    private TextField nameInputField;
    private TextField hostInputField;
    private TextField portInputField;

    private CannonGoClient client;
    private JoinResponse joinResp;

    public TitleScreen(CannonGdx incGame) {
        game = incGame;
        OrthographicCamera camera = new OrthographicCamera();
        stage = new Stage(new FitViewport(game.width, game.height, camera));

        int col_width = game.width / 12;

        final Label nameLabel = new Label("Player Name:", CannonGdx.skin,"default");
        nameInputField = new TextField("", CannonGdx.skin);

        Label hostLabel = new Label("Host:", CannonGdx.skin,"default");
        Label portLabel = new Label("Port:", CannonGdx.skin,"default");
        hostInputField = new TextField("", CannonGdx.skin);
        portInputField = new TextField("", CannonGdx.skin);

        Table outerTable = new Table(game.skin);
        Table leftTable = new Table(game.skin);
        Table remoteHostTable = new Table(game.skin);

        leftTable.add(nameLabel).row();
        leftTable.add(nameInputField).row();

        remoteHostTable.add(hostLabel).padRight(20f);
        remoteHostTable.add(hostInputField).padBottom(5f);
        remoteHostTable.row();
        remoteHostTable.add(portLabel);
        remoteHostTable.add(portInputField).width(col_width).padBottom(5f);

        final TextButton joinButton = new TextButton("Join", CannonGdx.skin,"default");
        joinButton.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                final String name = nameInputField.getText();
                final String host = hostInputField.getText();
                final String port = portInputField.getText();
                if (name.length() > 0) {
                    if (host.length() > 0 && port.length() > 0) {
                         client = new CannonGoClient(host, Integer.parseInt(port));
                    } else {
                        client = new CannonGoClient();
                    }
                    final Dialog d = new Dialog("Please Wait...", CannonGdx.skin);
                    d.setFillParent(true);
                    stage.addActor(d);
                    new Thread() {
                        public void run(){
                            joinResp = client.join(name);
//                            Gdx.app.log("info", joinResp.getSecret() + " err: " + joinResp.getError() + "<-");
                        }
                    }.start();
                }
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });

        leftTable.add(joinButton);
        outerTable.add(leftTable).fill().prefWidth(game.width/2);
        outerTable.add(remoteHostTable).fill().prefWidth(game.width/2);
        outerTable.setFillParent(true);
        stage.addActor(outerTable);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (joinResp != null){
            if (joinResp.getError().length() == 0){
                Gdx.app.log("info", "switching to gamescreen");
                client.setUser(nameInputField.getText().trim());
                client.setSecret(joinResp.getSecret());
                game.setScreen(new GameScreen(game, client));
                dispose();
            } else {
                joinResp = null;
            }
        }
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
