package com.donachys.cannongdx.android;

import android.os.Bundle;
import android.content.Context;


import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import com.donachys.cannongdx.CannonGdx;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		int width = 1680;
		int height = 945;
		initialize(new CannonGdx(width, height), config);
	}

}
